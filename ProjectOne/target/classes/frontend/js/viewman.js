document.getElementById("View").addEventListener("click", move1);
document.getElementById("Submit").addEventListener("click", move2 );
document.getElementById("navbarLink3").addEventListener("click", move3);

document.onload = reqGet1();

function move1(){
	window.location.href = 'http://localhost:9012/html/manager.html';
};

function move2(){
	window.location.href = 'http://localhost:9012/html/addReimbMan.html';

};

function move3(){
	window.location.href = 'http://localhost:9012/home';

};


document.getElementById("All").addEventListener("click", reqGet1);
document.getElementById("Approved").addEventListener("click", reqGet2);
document.getElementById("Denied").addEventListener("click", reqGet3);
document.getElementById("Pending").addEventListener("click", reqGet4);


function reqGet1(){	
	var url = '/FinanceManagerHome/viewreq/ALL'
	url = url.replace('"','');
	url = url.replace('"','');
	fetch(url,{
		method: 'GET'
	}).then((resp) => resp.json()).then(function(data){
		reqDisp(data);
	});
};

function reqGet2(){	
	var url = '/FinanceManagerHome/viewreq/APPROVED'
	url = url.replace('"','');
	url = url.replace('"','');
	fetch(url,{
		method: 'GET'
	}).then((resp) => resp.json()).then(function(data){
		reqDisp(data);
	});
};

function reqGet3(){	
	var url = '/FinanceManagerHome/viewreq/DENIED'
	url = url.replace('"','');
	url = url.replace('"','');
	fetch(url,{
		method: 'GET'
	}).then((resp) => resp.json()).then(function(data){
		reqDisp(data);
	});
};

function reqGet4(){	
	var url = '/FinanceManagerHome/viewreq/PENDING'
	url = url.replace('"','');
	url = url.replace('"','');
	fetch(url,{
		method: 'GET'
	}).then((resp) => resp.json()).then(function(data){
		reqDisp(data);
	});
};

function reqDisp(data) {
	let parent = document.getElementById('RequestTable');
	for(var i = parent.rows.length - 1; i > 0; i--)
	{
    	parent.deleteRow(i);
	}
	if(data.length == 0){
		return;
	};
	
	let row = 0;
	var reqTable = document.getElementById("RequestTable");
	let tBody = document.createElement('tbody');
	tBody.id = 'tbody';
	for(i of data){
		let NewRow = document.createElement('tr');
		let c1 = document.createElement('td');
		c1.appendChild(document.createTextNode(i.reqNum));
		NewRow.appendChild(c1);
		
		let c2 = document.createElement('td');
		c2.appendChild(document.createTextNode(i.username));
		NewRow.appendChild(c2);
		
		let c3 = document.createElement('td');
		c3.appendChild(document.createTextNode(i.ammount));
		NewRow.appendChild(c3);
		
		let c4 = document.createElement('td');
		c4.appendChild(document.createTextNode(i.description));
		NewRow.appendChild(c4);
		
		let c5 = document.createElement('td');
		c5.appendChild(document.createTextNode(i.status));
		NewRow.appendChild(c5);
		
		let c6 = document.createElement('td');
		c6.appendChild(document.createTextNode(i.submitted));
		NewRow.appendChild(c6);
		
		let c7 = document.createElement('td');
		c7.appendChild(document.createTextNode(i.dateResolved));
		NewRow.appendChild(c7);
		
		let c8 = document.createElement('td');
		c8.appendChild(document.createTextNode(i.resBy));
		NewRow.appendChild(c8);
		
		if(i.status == 'PENDING'){
			var selectAD = document.createElement('select');
			selectAD.name = "appDeny";
			selectAD.id = "selectAD"+row;
			var option1 = document.createElement('option')
			option1.value= 'NONE';
			option1.text = "Please Select";
			var option2 = document.createElement('option')
			option2.value= 'APPROVED'+i.reqNum;
			option2.text = "Approve";
			var option3 = document.createElement('option')
			option3.value= 'DENIED'+i.reqNum;
			option3.text = "Deny";
			selectAD.appendChild(option1);
			selectAD.appendChild(option2);
			selectAD.appendChild(option3);
			NewRow.appendChild(selectAD);
			row = row+1;
		}else{ 
			let c10 = document.createElement('td');
			c10.appendChild(document.createTextNode(''));
			NewRow.appendChild(c10);
		}
		tBody.appendChild(NewRow);
		
	}
	reqTable.appendChild(tBody);
	for( let i = 0; i< row; i++){
		document.getElementById("selectAD"+i).addEventListener("change", appDenReq );
	}
};

function appDenReq(data){
	var e = JSON.stringify(document.getElementById(data.target.id).value);
	let i = e.substring(1,e.length-2);
	let j = e.substring(e.length-2,e.length-1);
  let formData = new FormData();
  formData.append('id',j);
  formData.append('newStat',i );
	console.log(i + " " + j);
	var url = '/FinanceManagerHome/Change';	
	fetch(url,{
		method: 'POST',
    body: formData
	});
  location.reload();
};