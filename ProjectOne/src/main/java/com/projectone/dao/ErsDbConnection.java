package com.projectone.dao;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ErsDbConnection {

  private String url = "jdbc:mariadb://database-1.cfq1lvewpiml.us-east-2.rds.amazonaws.com:3306/projectonedb";
	private String username = "projectoneadmin";
	private String password = "projectonepass";
	
	public Connection getDBConnection() throws SQLException {
		return DriverManager.getConnection(url,username,password);  		
	}
  
}
