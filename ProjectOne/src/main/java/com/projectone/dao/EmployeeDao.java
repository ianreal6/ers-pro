package com.projectone.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;


import com.projectone.model.Employee;
import com.projectone.util.HibernateUtil;

public class EmployeeDao {

  private HibernateUtil hUtil = new HibernateUtil();
  private static ErsDbConnection edc;

  public EmployeeDao() {	
  }

  public EmployeeDao(HibernateUtil hUtil){
    super();
    this.hUtil = hUtil;
  }

  public EmployeeDao(ErsDbConnection edcNew){
		edc = edcNew;
	}

  public List<Employee> getAllEmployees(){
    Session ses = hUtil.getSession();
    return ses.createQuery("select * from Employee", Employee.class).list();
  }

  public void insert(Employee emp) {
		Session ses = hUtil.getSession();
		Transaction tx = ses.beginTransaction();
		ses.save(emp);
		tx.commit();
	}

	public void update(Employee emp) {
		Session ses = hUtil.getSession();
		Transaction tx = ses.beginTransaction();
		ses.update(emp);
		tx.commit();
	}

	public Employee selectById(int id) {
		Session ses = hUtil.getSession();
		Employee emp = ses.get(Employee.class, id);
		return emp;
  }

  public Employee findByUsername(String username){
    Session ses = hUtil.getSession();
    List<Employee> empList = ses.createQuery("from Employee where ersUsername = '"+username+"'", Employee.class).list();
    if(empList.size()== 0) {
      return null;
    }
    return empList.get(0);
  }
}
