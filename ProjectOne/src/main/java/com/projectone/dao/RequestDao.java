package com.projectone.dao;

import java.util.List;
import java.util.ArrayList;

import java.time.LocalDate;
import java.time.ZoneId;


import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.projectone.model.Request;
import com.projectone.util.HibernateUtil;

public class RequestDao {

  private HibernateUtil hUtil = new HibernateUtil();
  private static ErsDbConnection edc;

  public RequestDao() {	
  }

  public RequestDao(HibernateUtil hUtil){
    super();
    this.hUtil = hUtil;
  }

  public RequestDao(ErsDbConnection edcNew){
		edc = edcNew;
	}


  public void insert(Request req) {
		Session ses = hUtil.getSession();
		Transaction tx = ses.beginTransaction();
		ses.save(req);
		tx.commit();
	}

	public void update(Request req) {
		Session ses = hUtil.getSession();
		Transaction tx = ses.beginTransaction();
		ses.update(req);
		tx.commit();
	}

	public Request selectById(int id) {
		Session ses = hUtil.getSession();
		Request req = ses.get(Request.class, id);
		return req;
  }

  public List<Request> findByUsername(String username){
		Session ses = hUtil.getSession();
		List<Request> reqList = ses.createQuery("from Request where username ='" + username + "'", Request.class).list();
		if(reqList.size() == 0) {
			return null;
		}
		return reqList;
  }

  public List<Request> findByStatus(String status){
		Session ses = hUtil.getSession();
		List<Request> reqNull = new ArrayList<Request>();
		if(status.equals("ALL")) {
			ses.beginTransaction().commit();
			List<Request> reqList1 = ses.createQuery("FROM Request WHERE reimb_status ='PENDING'", Request.class).list();
			List<Request> reqList2 = ses.createQuery("FROM Request WHERE reimb_status ='APPROVED'", Request.class).list();
			List<Request> reqList3 = ses.createQuery("FROM Request WHERE reimb_status ='DENIED'", Request.class).list();
			List<Request> reqList = new ArrayList<Request>();
			reqList.addAll(reqList1);
			reqList.addAll(reqList2);
			reqList.addAll(reqList3);
			if(reqList.size() == 0) {
				
				return reqNull;
			}else{
				return reqList;
			}
		}else {
			List<Request> reqList = ses.createQuery("FROM Request WHERE reimb_status ='" + status + "'", Request.class).list();
			if(reqList.size() == 0) {
				return reqNull;
			}
			return reqList;
		}
  }

  public Request findById(int id) {
		Session ses = hUtil.getSession();
		List<Request> reqList = ses.createQuery("FROM Request WHERE reimb_ticket_num = '" + id+ "'", Request.class).list();
		if(reqList.size() == 0) {
			return null;
		}
		return reqList.get(0);
	}

  public void appOrDeny(int id, String newStat, String resBy){		
		ZoneId zonedId = ZoneId.of( "America/Montreal" );
		LocalDate today = LocalDate.now( zonedId );
		String date = today.toString();
		Request req = new Request();
		req = findById(id);
		req.setStatus(newStat);
		req.setDateResolved(date);
		req.setResBy(resBy);
		Session ses = hUtil.getSession();
		Transaction tx = ses.beginTransaction();
		String update1s = "update Request r SET r.status = '"+ newStat + "'  WHERE r.reqNum = "+Integer.toString(id);
		String update2s = "update Request r set r.dateResolved = '"+ date + "' where r.reqNum = "+Integer.toString(id);
		String update3s = "update Request r set r.resBy = '"+ resBy + "' where r.reqNum = "+Integer.toString(id);
		Query update1 = ses.createQuery(update1s);
		Query update2 = ses.createQuery(update2s);
		Query update3 = ses.createQuery(update3s);
		update1.executeUpdate();
		update2.executeUpdate();
		update3.executeUpdate();
		tx.commit();
	}
}