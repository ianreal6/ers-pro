package com.projectone.service;

import com.projectone.dao.EmployeeDao;
import com.projectone.model.Employee;

import org.jasypt.util.password.BasicPasswordEncryptor;

public class EmployeeService {
  public static EmployeeDao eDao;
  private final BasicPasswordEncryptor bpe = new BasicPasswordEncryptor();

  public EmployeeService(){
  }

  public EmployeeService(EmployeeDao setDao){
    super();
    eDao = setDao;
  }
  
  public Employee getEmployee(String username){
    return eDao.findByUsername(username);
  }

  public void insertEmployee(Employee employee){
    Employee emp = new Employee(employee.getErsUsername(), 
      bpe.encryptPassword(employee.getErsPassword()), 
      employee.getUserFirstName(), 
      employee.getUserLastName(), 
      employee.getUserEmail(),
      employee.getUserRoleId());
    eDao.insert(emp);
  }

  public boolean verifyPassword(String username, String password) {
    return bpe.checkPassword(password, getEmployee(username).getErsPassword());
  }

  public boolean isMan(String username) {
    return getEmployee(username).getUserRoleId()==1;
  }

}
