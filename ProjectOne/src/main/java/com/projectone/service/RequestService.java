package com.projectone.service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import com.projectone.dao.RequestDao;
import com.projectone.dto.RequestDto;
import com.projectone.model.Request;

public class RequestService {
  public static RequestDao rDao;

  public RequestService() {
  }

  public RequestService(RequestDao newDao){
    super();
    rDao = newDao;
  }

  public List<Request> findByUsername(String username){
    return rDao.findByUsername(username);
  }

  public List<Request> findByStatus(String status){
    return rDao.findByStatus(status);
  }

  public Request findById(int id){
    return rDao.findById(id);
  }

  public void appOrDeny(int id, String newStat, String resBy){
    rDao.appOrDeny(id, newStat, resBy);
  }

  public void addReq(String username, RequestDto rDto){
		String date = LocalDate.now( ZoneId.of( "America/Montreal" ) ).toString();
    Request req = new Request(username, rDto.amount, date, "", "PENDING", "", rDto.description);
    rDao.insert(req);
  }
}
