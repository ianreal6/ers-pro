package com.projectone.dto;

public class RequestDto {

  public double amount;

  public String description;

  public RequestDto(String amount, String description){
    this.amount = Double.parseDouble(amount);
    this.description = description;
  }
}
