package com.projectone;

// import java.io.File;
// import java.util.concurrent.TimeUnit;

// import org.openqa.selenium.By;
// import org.openqa.selenium.WebDriver;
// import org.openqa.selenium.WebElement;
// import org.openqa.selenium.chrome.ChromeDriver;

import com.projectone.controller.EmployeeController;
import com.projectone.controller.RequestController;
import com.projectone.dao.EmployeeDao;
import com.projectone.dao.ErsDbConnection;
import com.projectone.dao.RequestDao;
import com.projectone.model.Employee;
import com.projectone.model.Request;
import com.projectone.service.EmployeeService;
import com.projectone.service.RequestService;
import com.projectone.util.HibernateUtil;

import io.javalin.Javalin;


public class MainDriver {

  public static HibernateUtil hUtil = new HibernateUtil();
  public static EmployeeDao eDao = new EmployeeDao(hUtil);
  public static EmployeeService eServ = new EmployeeService(eDao);
  public static RequestDao rDao = new RequestDao(hUtil);
  public static RequestService rServ = new RequestService(rDao);
  
  public static void main (String[] args) throws InterruptedException{
  populateDb();

		EmployeeController eCon = new EmployeeController(new EmployeeService(new EmployeeDao(new ErsDbConnection())));
		RequestController rCon = new RequestController(new RequestService(new RequestDao(new ErsDbConnection())));
		Javalin app = Javalin.create(config ->{
			config.addStaticFiles("/frontend");
			config.enableCorsForAllOrigins();
    });
    app.start(9012);
    app.get("/home",  eCon.HOME);
    app.post("/login", eCon.POSTLOGIN);

    app.get("/FinanceManagerHome/viewreq/:status", rCon.VIEWREQBY);
		app.post("/addRequest", rCon.ADDREQ);
		app.post("/FinanceManagerHome/Change/", rCon.APPORDENY);
		app.get("/EmployeeHome/", rCon.EMPLOYEEVIEWREQ);

    app.exception(NullPointerException.class, (e, ctx)->{
      ctx.status(404);
			ctx.redirect("/html/index.html");
		});
  }

  public static void populateDb(){
    Employee emp1 = new Employee("ismith", "myPass", "Ian", "Smith", "emp1@gmail.com", 0);
		Employee emp2 = new Employee("vdurant", "thepass", "Virginie", "Meyer", "emp2@gmail.com", 1);
    Employee emp3 = new Employee("bsmith", "password", "Bob", "Smith", "info@gmail.com", 1);
    eServ.insertEmployee(emp1);
    eServ.insertEmployee(emp2);
    eServ.insertEmployee(emp3);

    Request req1 = new Request("vdurant",100.1, "12/22/2020", "", "PENDING", "", "TRAVEL");
		Request req2 = new Request("vdurant",100.5, "12/22/2020", "", "PENDING", "", "LODGING");
		Request req3 = new Request("bsmith",100.4, "12/22/2020", "1/1/2021", "APPROVED", "ireal", "FOOD");
		Request req4 = new Request("bsmith",100.3, "12/22/2020", "", "DENIED", "ireal", "FOOD");
		Request req5 = new Request("bsmith",100.2, "12/22/2020", "1/10/2021", "APPROVED", "ireal", "OTHER");
		Request req6 = new Request("ismith",300.50, "11/32/2020", "", "PENDING", "", "LODGING");

    rDao.insert(req1);
		rDao.insert(req2);
		rDao.insert(req3);
		rDao.insert(req4);
		rDao.insert(req5);
		rDao.insert(req6);
		
  }
}
