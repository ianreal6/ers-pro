package com.projectone.controller;

import com.projectone.dto.RequestDto;
import com.projectone.dao.EmployeeDao;
import com.projectone.dao.ErsDbConnection;
import com.projectone.util.HibernateUtil;

import java.util.List;

import com.projectone.dto.AppDenDto;
import com.projectone.service.EmployeeService;
import com.projectone.service.RequestService;
import com.projectone.model.Request;

import io.javalin.http.Handler;

public class RequestController {
  public static HibernateUtil hUtil = new HibernateUtil();
  public static EmployeeDao eDao = new EmployeeDao(hUtil);
  private RequestService rServ;
  private EmployeeService eServ = new EmployeeService(new EmployeeDao(new ErsDbConnection()));
  public final String homePage = "/html/index.html";
  public final String manPage = "/html/manager.html";
  public final String empPage = "/html/employee.html";
  public final String viewPage = "/html/viewReimb.html";
  public final String addPage = "/html/addReimb.html";

  public RequestController() {

	}

  public RequestController(RequestService rServ){
    super();
    this.rServ = rServ;
  }

  public Handler APPORDENY = (ctx) -> {
    String uName = "";
    uName = ctx.cookieStore("empUser");
    AppDenDto adDto = new AppDenDto(ctx.formParam("id"), ctx.formParam("newStat"));
    if(eServ.isMan(uName)){
      rServ.appOrDeny(adDto.id, adDto.newStat, uName);
      ctx.redirect(manPage);
    }else{
      ctx.redirect(homePage);
    }
  };

  public Handler EMPLOYEEVIEWREQ = (ctx)->{
    String uName = "";
    uName = ctx.cookieStore("empUser");
    List<Request> reqList = rServ.findByUsername(uName);
    ctx.json(reqList);
  };

  public Handler ADDREQ = (ctx) ->{
    String uName = "";
    uName = ctx.cookieStore("empUser");
    RequestDto rDto = new RequestDto(ctx.formParam("amount"),ctx.formParam("description"));
    rServ.addReq(uName, rDto);
  };

  public Handler VIEWREQBY = (ctx) ->{
    String sort = ctx.pathParam("status");
    ctx.json(rServ.findByStatus(sort));
  };
}
