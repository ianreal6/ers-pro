package com.projectone.controller;

import com.projectone.model.Employee;
import com.projectone.service.EmployeeService;

import io.javalin.http.Handler;

public class EmployeeController {
  private EmployeeService eServ;
  public final String homePage = "/html/index.html";
  public final String manPage = "/html/manager.html";
  public final String empPage = "/html/employee.html";

  public EmployeeController() {
  }

  public EmployeeController(EmployeeService eServ) {
    super();
    this.eServ = eServ;
  }

  public Handler HOME = (ctx) ->{
    ctx.redirect(homePage);
    ctx.cookieStore("empUser", "blank" );
  };

  public Handler POSTLOGIN = (ctx) -> {
    String uName = ctx.formParam("username");
    String pWord = ctx.formParam("password");
    if(eServ.verifyPassword(uName, pWord)){
      if(eServ.isMan(uName)){
        ctx.cookieStore("empUser",uName );
        ctx.redirect(manPage);
      }else{
        ctx.cookieStore("empUser", uName );
        ctx.redirect(empPage);
      }
    }else{
      ctx.status(403);
    }
  };
}
