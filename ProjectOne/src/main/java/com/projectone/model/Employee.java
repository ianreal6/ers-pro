package com.projectone.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="ers_users")
public class Employee {
	
	@Id
	@Column(name="ers_username", unique = true, nullable = false)
	// if this was a number we would autogenerate the id by using this line   @GeneratedValue(strategy=GenerationType.AUTO)
	private String ersUsername;
	
	@Column(name="ers_password", nullable = false)
	private String ersPassword;
	
	@Column(name="user_first_name", nullable = false)
	private String userFirstName;
	
	@Column(name="user_last_name", nullable = false)
	private String userLastName;
	
	@Column(name="user_email", unique = true, nullable = false)
	private String userEmail;
	// to make specific constraings (like numbers being greater than something use @Column(name="", columnDefiniotn = "Number(10) CHECK(>0" or something like that
	// you can define the table ourself and use update instead
	@Column(name="user_role_id", nullable = false)
	private int userRoleId;

	public Employee(){
	}

	public Employee(String ersUsername, String ersPassword, String userFirstName, String userLastName, String userEmail,
			int userRoleId) {
		super();
		this.ersUsername = ersUsername;
		this.ersPassword = ersPassword;
		this.userFirstName = userFirstName;
		this.userLastName = userLastName;
		this.userEmail = userEmail;
		this.userRoleId = userRoleId;
  }


	public String getErsUsername() {
		return ersUsername;
	}

	public void setErsUsername(String ersUsername) {
		this.ersUsername = ersUsername;
	}

	public String getErsPassword() {
		return ersPassword;
	}

	public void setErsPassword(String ersPassword) {
		this.ersPassword = ersPassword;
	}

	public String getUserFirstName() {
		return userFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public int getUserRoleId() {
		return userRoleId;
	}

	public void setUserRoleId(int userRoleId) {
		this.userRoleId = userRoleId;
	}

	@Override
	public String toString() {
		return "Employee [ersUsername=" + ersUsername + ", userFirstName="
				+ userFirstName + ", userLastName=" + userLastName + ", userEmail=" + userEmail + ", userRoleId="
				+ userRoleId + "]";
	}
}
