package com.projectone.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ers_reimbursment")
public class Request {

	@Id
	@Column(name="reimb_ticket_num")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int reqNum;
	
	@Column(name="ers_username")
	public String username;
	
	@Column(name="reimb_amount")
	public double amount;
	
	@Column(name="reimb_submitted") // date sbmitted
	public String submitted;
	
	@Column(name="reimb_resolved")
	public String dateResolved;
	
	@Column(name = "reimb_status")
	public String status;
	
	@Column(name = "reimb_resolved_by")
	public String resBy;
	
	@Column(name="reimb_description")
	public String description;
	

	public Request() {
	}

	public Request(int reqNum, String username, double amount, String submitted, String dateResolved, String status, String resBy, String description){
		super();
		this.reqNum = reqNum;
		this.username = username;
		this.amount = amount;
		this.submitted = submitted;
		this.dateResolved = dateResolved;
		this.status = status;
		this.resBy	= resBy;
		this.description = description;
	}

	public Request(String username, double amount, String submitted, String dateResolved, String status, String resBy, String description) {
		super();
		this.username = username;
		this.amount = amount;
		this.submitted = submitted;
		this.dateResolved = dateResolved;
		this.status = status;
		this.resBy = resBy;
		this.description = description;
	}

	public Request(String username, double amount, String submitted, String description) {
		super();
		this.username = username;
		this.amount = amount;
		this.submitted = submitted;
		this.status = "PENDING";
		this.description = description;
	}

	public int getReqNum() {
		return reqNum;
	}

	public void setReqNum(int reqNum) {
		this.reqNum = reqNum;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getSubmitted() {
		return submitted;
	}

	public void setSubmitted(String submitted) {
		this.submitted = submitted;
	}

	public String getDateResolved() {
		return dateResolved;
	}

	public void setDateResolved(String dateResolved) {
		this.dateResolved = dateResolved;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResBy() {
		return resBy;
	}

	public void setResBy(String resBy) {
		this.resBy = resBy;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Request [reqNum=" + reqNum + ", username=" + username + ", amount=" + amount + ", submitted="
				+ submitted + ", dateResolved=" + dateResolved + ", status=" + status + ", resBy=" + resBy
				+ ", description=" + description + ", recipt=" + "]";
	}
}
