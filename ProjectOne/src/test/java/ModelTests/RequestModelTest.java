package ModelTests;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;
import java.util.stream.Stream;

import com.projectone.model.Request;
import com.projectone.model.Employee;
import com.projectone.dto.RequestDto;

import org.jetbrains.annotations.TestOnly;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class RequestModelTest {
  private Employee manager;
  private Employee nonmanager;
  private Request req1;

  private String username1 = "TestCaseUserName";
  private String password1 = "ASuperStrongPassword";
  private String email1 = "thisisanemail@gmail.com";
  private String fName1 = "Tom";
  private String lName1 = "Hill";
  private int roleManager1 = 1;

  private String username2 = "TestCaseUserName";
  private String password2 = "ASuperStrongPassword";
  private String email2 = "thisisanemail@gmail.com";
  private String fName2 = "Tom";
  private String lName2 = "Hill";
  private int roleManager2 = 0;

  private String reqUserName = "TestCaseUserName";
  private double reqAmmount = 62.58;
  private String date = "2/18/2021";
  private String description = "LODGING";

  @BeforeEach
  public void setup(){
    manager = new Employee(username1, password1, fName1,lName1,email1,roleManager1);
    nonmanager = new Employee(username2, password2, fName2,lName2,email2,roleManager2);
    req1 = new Request(reqUserName, reqAmmount, date, description);
  }

  @Test
  public void ManagerNotNull(){
    assertNotNull(req1);
    assertNotNull(manager);
    assertNotNull(nonmanager);
  }

  @Test
  public void RequestGetMethods(){
    assertEquals(0,req1.getReqNum());
    assertEquals(reqUserName,req1.getUsername());
    assertEquals(reqAmmount,req1.getAmount());
    assertEquals(date,req1.getSubmitted());
    assertEquals("PENDING",req1.getStatus());
    assertEquals(description,req1.getDescription());
    assertNull(req1.getResBy());
    assertNull(req1.getDateResolved());
  }

  @Test
  public void RequestSetMethods(){
    req1.setUsername("newUName");
    req1.setAmount(100.10);
    req1.setSubmitted("2/22/2021");
    req1.setStatus("APPROVED");
    req1.setDescription("OTHER");
    req1.setResBy("TestCaseUserName");
    req1.setDateResolved("2/23/2021");

    assertEquals("newUName",req1.getUsername());
    assertEquals(100.10,req1.getAmount());
    assertEquals("2/22/2021",req1.getSubmitted());
    assertEquals("APPROVED",req1.getStatus());
    assertEquals("OTHER",req1.getDescription());
    assertEquals("TestCaseUserName",req1.getResBy());
    assertEquals("2/23/2021",req1.getDateResolved());
  }

}
