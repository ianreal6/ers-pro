package ModelTests;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;
import java.util.stream.Stream;

import com.projectone.dao.EmployeeDao;
import com.projectone.service.EmployeeService;
import com.projectone.model.Employee;
//import com.projectone.dto.EmployeeDto;

import org.jetbrains.annotations.TestOnly;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;


import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class EmployeeModelTest {
  private Employee manager;
  private Employee nonmanager;

  private String username1 = "TestCaseUserName";
  private String username2 = "AnotherUsername";
  private String password1 = "ASuperStrongPassword";
  private String password2 = "anEven5tr0ng3rPA22w0rd!";
  private String email1 = "thisisanemail@gmail.com";
  private String email2 = "bigbrain@hotmail.com";
  private String fName1 = "Tom";
  private String fName2 = "Bobby";
  private String lName1 = "Riddle";
  private String lName2 = "Hill";
  private int roleManager = 1;
  private int roleNonMan = 0;

  @BeforeEach
  public void before(){
    manager = new Employee(username1, password1, fName1,lName1,email1,roleManager);
    nonmanager = new Employee(username2, password2, fName2,lName2,email2,roleNonMan);
  }
  @Test
  public void ManagerNotNull(){
    assertNotNull(manager);
  }
  
  @Test
  public void NonManagerNotNull(){
    assertNotNull(nonmanager);
  }

  @Test
  public void EmployeeManagerGetMethods(){
    assertEquals(username1,manager.getErsUsername());
    assertEquals(password1,manager.getErsPassword());
    assertEquals(fName1,manager.getUserFirstName());
    assertEquals(lName1,manager.getUserLastName());
    assertEquals(email1,manager.getUserEmail());
    assertEquals(1,manager.getUserRoleId());

    assertEquals(username2,nonmanager.getErsUsername());
    assertEquals(password2,nonmanager.getErsPassword());
    assertEquals(fName2,nonmanager.getUserFirstName());
    assertEquals(lName2,nonmanager.getUserLastName());
    assertEquals(email2,nonmanager.getUserEmail());
    assertEquals(0,nonmanager.getUserRoleId());
  }

  @Test
  public void EmployeeManagerSetMethods(){
    manager.setErsUsername("NewMUName");
    manager.setErsPassword("NewMPass");
    manager.setUserFirstName("NewMFName");
    manager.setUserLastName("NewMLName");
    manager.setUserRoleId(0);

    assertEquals("NewMUName",manager.getErsUsername());
    assertEquals("NewMPass",manager.getErsPassword());
    assertEquals("NewMFName",manager.getUserFirstName());
    assertEquals("NewMLName",manager.getUserLastName());
    assertEquals(0,manager.getUserRoleId());

    nonmanager.setErsUsername("NewNUName");
    nonmanager.setErsPassword("NewNPAss");
    nonmanager.setUserFirstName("NewNFName");
    nonmanager.setUserLastName("NewNLName");
    nonmanager.setUserRoleId(1);

    assertEquals("NewNUName",nonmanager.getErsUsername());
    assertEquals("NewNPAss",nonmanager.getErsPassword());
    assertEquals("NewNFName",nonmanager.getUserFirstName());
    assertEquals("NewNLName",nonmanager.getUserLastName());
    assertEquals(1,nonmanager.getUserRoleId());
  }
}
