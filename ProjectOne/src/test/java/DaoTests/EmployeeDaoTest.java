package DaoTests;

import static org.junit.jupiter.api.Assertions.*;

import com.projectone.dao.EmployeeDao;
import com.projectone.util.HibernateUtil;

import org.junit.jupiter.api.Test;

import org.mockito.Mock;

public class EmployeeDaoTest{

  @Mock
  private HibernateUtil hUtil;
  
  EmployeeDao eDao = new EmployeeDao(hUtil);


  @Test
  public void daoNotNull(){
    assertNotNull(eDao);
  }
}
