package ServiceTests;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;


import net.bytebuddy.agent.builder.AgentBuilder.CircularityLock.Global;

import com.projectone.dao.RequestDao;
import com.projectone.service.RequestService;
import com.projectone.model.Request;
import com.projectone.model.Employee;
import com.projectone.dto.RequestDto;

import org.jetbrains.annotations.TestOnly;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;


@ExtendWith(MockitoExtension.class)
public class RequestServiceTest {
  @Mock
  private RequestDao rDao;
  @InjectMocks
  RequestService rServ;

  Employee nonManager = new Employee("ismith", "myPass", "Ian", "Smith", "emp1@gmail.com", 0);
	Employee manager = new Employee("vMeyer", "thepass", "Virginie", "Meyer", "emp2@gmail.com", 1);


  Request req1a = new Request("ismith",100.1, "2/22/2021", "", "PENDING", "", "TRAVEL");
  Request req1b = new Request("vMeyer",100.1, "12/22/2020", "", "PENDING", "", "TRAVEL");
  Request req2a = new Request("vMeyer",100.5, "12/22/2020", "", "APPROVED", "", "LODGING");
  Request req2b = new Request("vMeyer",100.5, "12/22/2020", "", "APPROVED", "", "LODGING");
  Request req2c = new Request("vMeyer",100.5, "12/22/2020", "", "APPROVED", "", "LODGING");
  Request req3a = new Request("ismith",100.4, "12/22/2020", "1/1/2021", "DENIED", "ireal", "FOOD");
  Request req3b = new Request("ismith",100.4, "12/22/2020", "1/1/2021", "DENIED", "ireal", "FOOD");
  Request req3c = new Request("ismith",100.4, "12/22/2020", "1/1/2021", "DENIED", "ireal", "FOOD");
  Request req3d = new Request("ismith",100.4, "12/22/2020", "1/1/2021", "DENIED", "ireal", "FOOD");

  private ArrayList<Request> appList,denList,pendList,allList,userList;

  @BeforeEach
  public void setupEntries(){
    //setup variable lists
    appList = new ArrayList<>();
    appList.add(req2a);
    appList.add(req2b);
    appList.add(req2c);

    denList = new ArrayList<>();
    denList.add(req3a);
    denList.add(req3b);
    denList.add(req3c);
    denList.add(req3d);

    pendList = new ArrayList<>();
    pendList.add(req1a);
    pendList.add(req1b);

    allList = new ArrayList<>();
    allList.add(req2a);
    allList.add(req2b);
    allList.add(req2c);
    allList.add(req3a);
    allList.add(req3b);
    allList.add(req3c);
    allList.add(req3d);
    allList.add(req1a);
    allList.add(req1b);
    
    userList = new ArrayList<>();
    userList.add(req2a);
    userList.add(req2b);
    userList.add(req2c);
    userList.add(req1a);
    userList.add(req1b);

    //Request req3aApproved = new Request("ismith",100.1, "12/22/2021", "2/25/2021", "APPROVED", "vMeyer", "TRAVEL");
    //when(rDao.appOrDeny(6, "APPROVED", "vMeyer")).thenReturn(req3aApproved);
  }

  @Test
  public void findByStatusTests(){
    when(rDao.findByStatus("APPROVED")).thenReturn(appList);
    when(rDao.findByStatus("DENIED")).thenReturn(denList);
    when(rDao.findByStatus("PENDING")).thenReturn(pendList);
    when(rDao.findByStatus("ALL")).thenReturn(allList);
    assertArrayEquals( rServ.findByStatus("APPROVED").toArray(),appList.toArray());
    assertArrayEquals(denList.toArray(), rServ.findByStatus("DENIED").toArray());
    assertArrayEquals(pendList.toArray(), rServ.findByStatus("PENDING").toArray());
    assertArrayEquals(allList.toArray(), rServ.findByStatus("ALL").toArray());
  }

  @Test
  public void findByUserTest(){
    
    when(rDao.findByUsername("vMeyer")).thenReturn(userList);
    assertArrayEquals(userList.toArray(), rServ.findByUsername("vMeyer").toArray());
  }

  @Test
  public void findByIdTest(){
    when(rDao.findById(1)).thenReturn(req1a);
    assertEquals(rServ.findById(1),req1a );
  }
}
