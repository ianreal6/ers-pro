package ServiceTests;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import com.projectone.dao.EmployeeDao;
import com.projectone.service.EmployeeService;
import com.projectone.model.Employee;
//import com.projectone.dto.EmployeeDto;

import org.jasypt.util.password.BasicPasswordEncryptor;

import org.jetbrains.annotations.TestOnly;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;


@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTest {

  private final BasicPasswordEncryptor bpe = new BasicPasswordEncryptor();

  @Mock
  private EmployeeDao eDao;
  @InjectMocks
  EmployeeService eServ;

  Employee emp1 = new Employee("ismith", "myPass", "Ian", "Smith", "emp1@gmail.com", 0);
	Employee emp2 = new Employee("vdurant", "thepass", "Virginie", "Meyer", "emp2@gmail.com", 1);
  Employee emp3 = new Employee("bsmith", "password", "Bob", "Smith", "info@gmail.com", 1);
  Employee emp1wEncript = new Employee("ismith", bpe.encryptPassword("myPass"), "Ian", "Smith", "emp1@gmail.com", 0);

  @Test
  public void daoExists(){
    assertNotNull(eDao);
  }

  @Test
  public void servExists(){
    assertNotNull(eServ);
  }
  
  //mock getting employee this test assumes all dao layer methods work properly
  @Test
  public void hitsFindByUser(){
    when(eDao.findByUsername("ismith")).thenReturn(emp1);
    assertEquals(emp1, eServ.getEmployee("ismith"));
  }

  //Testing if Verify Password hits proper methods

  @Test
  public void verifyPassTest(){
    when(eDao.findByUsername("ismith")).thenReturn(emp1wEncript);
    assertEquals(true,eServ.verifyPassword("ismith", "myPass"));
    assertEquals(false,eServ.verifyPassword("ismith", "badPass"));
  }

  @Test
  public void isManTest(){
    when(eDao.findByUsername("ismith")).thenReturn(emp1);
    when(eDao.findByUsername("vdurant")).thenReturn(emp2);
    assertEquals(false, eServ.isMan("ismith"));
    assertEquals(true, eServ.isMan("vdurant"));
  }
}
